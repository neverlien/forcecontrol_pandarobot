#include "matplotlibcpp.h"
#include "read_from_csv.h"
#include "ostream_overwrite.h"

namespace plt = matplotlibcpp;



// int main()
// {
//     // Creating an object of CSVWriter
// 	CSVReader reader("example.csv");
 
// 	// Get the data from CSV File
// 	std::vector<std::vector<std::string> > dataList = reader.getData();

//     std::vector<double> input = {4.3,8.78, 6};
//     std::vector<double> output = { 1.5, 2.9, 3};
//     // std::vector<std::string> utput = { "hei", "hh"};

	
// 	//std::cout << dataList.size() <<  ": " << dataList;
//     // std::cout << input.size();
//     // std::cout << output.size();
//     // std::cout << utput.size();

//     plt::plot(output, input);
//     plt::show();

// 	return 0;
// }


int main()
{
    std::vector<std::vector<double>> x, y, z;
    for (double i = -5; i <= 5;  i += 0.25) {
        std::vector<double> x_row, y_row, z_row;
        for (double j = -5; j <= 5; j += 0.25) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(::std::sin(::std::hypot(i, j)));
        }
        x.push_back(x_row);
        y.push_back(y_row);
        z.push_back(z_row);
    }

    plt::plot_surface(x, y, z);
    plt::show();
}