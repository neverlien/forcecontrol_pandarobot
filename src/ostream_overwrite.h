#include <vector>
#include <iostream>

std::ostream& operator<<(std::ostream& os, const std::vector<std::string> &input)
{
	for (auto const& i: input) {
		os << i << " ";
	}
	return os;
}
std::ostream& operator<<(std::ostream& os, const std::vector<int> &input)
{
	for (auto const& i: input) {
		os << i << " ";
	}
	return os;
}
std::ostream& operator<<(std::ostream& os, const std::vector<std::vector<std::string> > &input)
{
	for (auto const& i: input) {
		os << i << " ";
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, const std::vector<double> &input)
{
	for (auto const& i: input) {
		os << i << " ";
	}
	return os;
}

